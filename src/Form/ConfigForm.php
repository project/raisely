<?php

namespace Drupal\raisely\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Path\PathValidatorInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\raisely\RaiselyApiManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ConfigForm.
 *
 * @package Drupal\raisely\Form
 */
class ConfigForm extends ConfigFormBase {

  /**
   * The path validator.
   *
   * @var \Drupal\Core\Path\PathValidatorInterface
   */
  protected PathValidatorInterface $pathValidator;

  /**
   * Raisely API Manager.
   *
   * @var \Drupal\raisely\RaiselyApiManager
   */
  protected RaiselyApiManager $raisely;

  /**
   * Constructs a \Drupal\system\ConfigFormBase object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Path\PathValidatorInterface $path_validator
   *   The path validator class.
   * @param \Drupal\raisely\RaiselyApiManager $raisely
   *   Raisely API manager.
   */
  public function __construct(ConfigFactoryInterface $config_factory, PathValidatorInterface $path_validator, RaiselyApiManager $raisely) {
    parent::__construct($config_factory);
    $this->pathValidator = $path_validator;
    $this->raisely = $raisely;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('path.validator'),
      $container->get('raisely.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'raisely.config',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'raisely_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('raisely.config');

    // Settings for Raisely API.
    $form['raisely_api'] = [
      '#type' => 'fieldset',
      '#collapsible' => FALSE,
      '#title' => $this->t('Raisely API'),
      '#tree' => TRUE,
    ];
    $form['raisely_api']['enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#default_value' => $config->get('raisely_api.enabled'),
    ];
    $form['raisely_api']['api_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Base URL'),
      '#description' => $this->t('URI of the Raisely API server without trailing slash.'),
      '#maxlength' => 255,
      '#size' => 255,
      '#default_value' => $config->get('raisely_api.api_url'),
    ];
    $form['raisely_api']['username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Raisely user name'),
      '#required' => TRUE,
      '#description' => $this->t('Provide admin username to connect to Raisely API.'),
      '#maxlength' => 255,
      '#size' => 255,
      '#default_value' => $config->get('raisely_api.username'),
    ];
    $form['raisely_api']['password'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Raisely password'),
      '#required' => TRUE,
      '#description' => $this->t('Password for the provided username.'),
      '#maxlength' => 255,
      '#size' => 255,
      '#default_value' => $config->get('raisely_api.password'),
    ];
    $form['raisely_api']['verbose'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Verbose logs'),
      '#default_value' => $config->get('raisely_api.verbose'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $raisely_api_url = $form_state->getValue('raisely_api')['api_url'];

    // Validate API base url.
    if ($raisely_api_url !== '' && !$this->pathValidator->isValid($raisely_api_url)) {
      $form_state->setErrorByName('raisely_api[api_url', $this->t('API Base Url should be a valid url.'));
    }

    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $values = $form_state->getValue('raisely_api');

    $this->config('raisely.config')
      ->set('raisely_api', $values)
      ->save();

    $this->raisely->init($values['username'], $values['password'], TRUE);
  }

}
