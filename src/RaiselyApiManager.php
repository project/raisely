<?php

namespace Drupal\raisely;

use Drupal\Core\Config\ImmutableConfig;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Logger\RfcLogLevel;
use Drupal\Core\State\StateInterface;
use GuzzleHttp\Client as GuzzleClient;

/**
 * Class RaiselyApiManager.
 *
 * @method \Drupal\raisely\Resources\RaiselyApi\Donations donations()
 * @method \Drupal\raisely\Resources\RaiselyApi\Donations campaigns()
 */
class RaiselyApiManager {

  /**
   * Default request timeout.
   */
  const REQUEST_TIMEOUT = 30;

  /**
   * Client instance.
   *
   * @var \GuzzleHttp\Client
   */
  public $client;

  /**
   * Raisely Api Url.
   *
   * @var string
   */
  private $apiUrl;

  /**
   * Enabled Raisely Api.
   *
   * @var bool
   */
  private $enabled;

  /**
   * Verbose logs.
   *
   * @var bool
   */
  private $verbose;

  /**
   * Raisely config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  private ImmutableConfig $config;

  /**
   * Logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  private LoggerChannelInterface $logger;

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected StateInterface $state;

  /**
   * The time.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected TimeInterface $time;

  /**
   * Constructs a new RaiselyApiManager service instance.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger_channel
   *   The logger factory.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time.
   */
  public function __construct(ConfigFactoryInterface $config_factory, LoggerChannelInterface $logger_channel, StateInterface $state, TimeInterface $time) {
    $this->config = $config_factory->get('raisely.config');
    $this->logger = $logger_channel;
    $this->state = $state;
    $this->time = $time;

    $this->apiUrl = $this->config->get('raisely_api.api_url');
    $this->enabled = $this->config->get('raisely_api.enabled');
    $this->verbose = $this->config->get('raisely_api.verbose');
    $username = $this->config->get('raisely_api.username');
    $password = $this->config->get('raisely_api.password');

    if (!$this->enabled) {
      $this->logger->info('Raisely API is disabled.');
      return NULL;
    }

    if (empty($this->apiUrl)) {
      $this->logger->error('Missing Raisely API configuration.');
      return NULL;
    }

    // Initialize the Client.
    $this->client = new GuzzleClient(['timeout' => self::REQUEST_TIMEOUT]);

    if ($username && $password) {
      $this->init($username, $password);
    }
  }

  /**
   * Initialize Raisely client by issuing API token.
   *
   * The API token is stored in Drupal store as "raisely.api_token" and
   * considered valid for 7 days.
   *
   * @param string $username
   *   Raisely username.
   * @param string $password
   *   Raisely password.
   * @param bool $force
   *   TRUE to force login request even if the API token is already set.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function init($username, $password, $force = FALSE) {
    $current_token = $this->state->get('raisely.api_token');
    $current_token_issued = $this->state->get('raisely.token_issued');

    if ($current_token
      && $current_token_issued
      && $current_token_issued > ($this->time->getRequestTime() - 7 * 24 * 60 * 60)
      && !$force
    ) {
      // Current token exists and valid.
      return;
    }

    $response = $this->client->request('POST', $this->apiUrl . '/login', [
      'headers' => [
        'Content-Type' => 'application/json',
      ],
      'body' => \GuzzleHttp\json_encode([
        'username' => $username,
        'password' => $password,
        'requestAdminToken' => TRUE,
      ]),
    ]);
    $content = $response->getBody()->getContents();
    $data = \GuzzleHttp\json_decode($content);

    $this->logger->info('Raisely API token has been updated.');
    $this->state->set('raisely.api_token', $data->token);
    $this->state->set('raisely.token_issued', $this->time->getRequestTime());
  }

  /**
   * Send the request.
   *
   * @param string $method
   *   The HTTP request verb.
   * @param string $endpoint
   *   The Raisely API endpoint (doesn't include base url).
   * @param array $options
   *   An array of options to send with the request.
   *
   * @return \Drupal\raisely\Response
   *   Response object.
   */
  public function request($method, $endpoint, array $options = []) {
    try {
      $url = $this->apiUrl . $endpoint;

      // Adds necessary headers.
      $options['headers']['accept'] = 'application/json';
      $options['headers']['Content-Type'] = 'application/json';
      $options['headers']['Authorization'] = "Bearer " . $this->state->get('raisely.api_token');

      if (!empty($this->verbose)) {
        $this->logger
          ->debug('Raisely API Request: %method %url. Query: <pre>%query</pre>  Data: <pre>%data</pre>', [
            '%method' => $method,
            '%url' => $url,
            '%data' => !empty($options['body']) ? json_encode(json_decode($options['body']), JSON_PRETTY_PRINT) : '',
            '%query' => !empty($options['query']) ? json_encode($options['query'], JSON_PRETTY_PRINT) : '',
          ]);
      }

      $response = $this->client->request($method, $url, $options);

      // Wrap to the custom Response class for useful functions.
      $wrapped_response = new Response($response);

      if (!empty($this->verbose)) {
        $data_for_log = $wrapped_response->toArray();
        $additional_message = 'Data :';
        if (
          is_array($data_for_log)
          && array_key_exists('data', $data_for_log)
          && count($data_for_log['data']) > 10) {
          $data_for_log['data'] = array_slice($data_for_log['data'], 0, 10);
          $additional_message = 'First 10 elements from data:';
        }
        $this->logger
          ->debug('Raisely API Response: %url. Status: %status. Query: <pre>%query</pre>  %additional_message <pre>%data</pre>', [
            '%url' => $url,
            '%status' => $wrapped_response->getStatusCode(),
            '%additional_message' => $additional_message,
            '%data' => json_encode($data_for_log, JSON_PRETTY_PRINT),
            '%query' => !empty($options['query']) ? json_encode($options['query'], JSON_PRETTY_PRINT) : '',
          ]);
      }
      return $wrapped_response;
    }
    catch (\Exception $e) {
      watchdog_exception('raisely', $e, NULL, [], RfcLogLevel::DEBUG);
      throw new \Exception($e->getMessage(), $e->getCode(), $e);
    }
  }

  /**
   * Return an instance of a Resource based on the method called.
   *
   * @param string $name
   *   Name of the method.
   * @param array $arguments
   *   Arguments that will be passed further.
   *
   * @return \Drupal\raisely\Resources\Resource
   *   Resource object.
   */
  public function __call($name, array $arguments = NULL) {
    $resource = 'Drupal\\raisely\\Resources\\RaiselyApi\\' . ucfirst($name);

    return new $resource($this);
  }

}
