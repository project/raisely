<?php

namespace Drupal\raisely\Resources;

use Drupal\raisely\RaiselyApiManager;

/**
 * Class Resource.
 */
abstract class Resource {
  /**
   * Client instance.
   *
   * @var \Drupal\raisely\RaiselyApiManager
   */
  protected $client;

  /**
   * Class constructor.
   *
   * @param \Drupal\raisely\RaiselyApiManager $client
   *   Client instance.
   */
  public function __construct(RaiselyApiManager $client) {
    $this->client = $client;
  }

}
