<?php

namespace Drupal\raisely\Resources\RaiselyApi;

use Drupal\raisely\Resources\Resource;

/**
 * Donations class.
 */
class Donations extends Resource {

  /**
   * Retrieves available donations from Raisely.
   *
   * @return \Drupal\raisely\Response
   *   Response object containing donations.
   */
  public function getAll(array $options = []) {
    $endpoint = "/donations";

    return $this->client->request('get', $endpoint, $options);
  }

}
