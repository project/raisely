<?php

namespace Drupal\raisely\Resources\RaiselyApi;

use Drupal\raisely\Resources\Resource;

/**
 * Campaigns class.
 */
class Campaigns extends Resource {

  /**
   * Retrieves available Campaigns from Raisely.
   *
   * @return \Drupal\raisely\Response
   *   Response object containing Campaigns.
   */
  public function getAll(array $options = []) {
    $endpoint = "/campaigns";

    return $this->client->request('get', $endpoint, $options);
  }

}
